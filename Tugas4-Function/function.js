//Jawaban No.1
console.log("=========== Jawaban No.1 ===========")
function teriak(){
    return "Halo Sanbers!"
}
console.log(teriak())
console.log("=============== END ================\n")

//Jawaban No.2
console.log("=========== Jawaban No.2 ===========")
function kalikan(n1,n2){
    hasil = n1*n2
    return hasil
}
var num1 = 12
var num2 = 4
var hasilkali = kalikan(num1,num2)

console.log(hasilkali)
console.log("=============== END ================\n")

//Jawaban No.3
console.log("=========== Jawaban No.3 ===========")

function introduce(){
    var kenalkan = "Nama saya "+ name +", umur saya "+ age +" tahun, alamat saya di "+ address +", dan saya punya hobby yaitu " + hobby +"!"
    return kenalkan

}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

console.log("=============== END ================\n")