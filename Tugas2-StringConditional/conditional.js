// //Jawaban Nomer 1 IF-ELSE
var nama = "Junaedi"
var peran ="Werewolf"

if(nama == "" && peran == ""){
    console.log("Nama Harus diisi!")
} else if(nama == "Jhon" && peran == ""){
    console.log("Halo Jhon, Pilih peranmu untuk memulai game!")
} else if(nama == "Jane" && peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}else if(nama == "Jenita" && peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(nama == "Junaedi" && peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, kamu akan memakan mangsa setiap malam!")
}

//=============================================================

//Jawaban Nomer 2 Switch - Case
var tanggal =10
var bulan = 12
var tahun = 2200
var namabulan;

switch(true){
case (tanggal < 1 || tanggal > 31): {
    console.log("Input Tanggal Salah !")
    break;
}
case (bulan < 1 || bulan > 12): {
    console.log("Input Bulan Salah !")
    break;
}
case (tahun < 1900 || tahun > 2200): {
    console.log("Tahun Tidak Terjangkau !")
    break;
}
default: {
    switch(true){
        case bulan == 1:
            namabulan = "Januari";
            break;
        case bulan == 2:
            namabulan = "Februari";
            break;
        case bulan == 3:
            namabulan = "Maret";
            break;
        case bulan == 4:
            namabulan = "April";
            break;
        case bulan == 5:
            namabulan = "Mei";
            break;
        case bulan == 6:
            namabulan = "Juni";
            break;
        case bulan == 7:
            namabulan = "Juli";
            break;
        case bulan == 8:
            namabulan = "Agustus";
            break;
        case bulan == 9:
            namabulan = "September";
            break;
        case bulan == 10:
            namabulan = "Oktober";
            break;
        case bulan == 11:
            namabulan = "November";
            break;
        case bulan == 12:
            namabulan = "Desember";
            break;
        default:
            break;
        }
        console.log(tanggal,"",namabulan,"",tahun)
        break;
    }
}



