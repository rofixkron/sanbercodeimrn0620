//No.1 Looping While
var coding = 2;

if(coding == 2) {
    console.log("LOOPING PERTAMA")
    while (coding < 22 ){
        console.log(coding+2,"- I Love Coding");
        coding+=2;
        if (coding == 20 ){
            console.log("LOOPING KEDUA")
            while(coding > 2){
                console.log(coding,"- I will become a mobile developer");
                coding-=2;
            }break; 
        }
    }
}

//------------------------------------------------

//No.2 Looping For
for (var angka = 1 ; angka <= 20; angka++){
    if(angka % 2 == 0){
        console.log(angka,"- Berkualitas")
    }else if (angka % 3 == 0)    {
        console.log(angka,"- I Love Coding")
    }else{
        console.log(angka,"- Santai")
    }
} 

//-----------------------------------------------------

//No. 3 Membuat Persegi Panjang
for(var i = 0; i< 4;i++){
    var p='';
    for(var j = 0; j < 8; j++){
        p+='#'
    }
    console.log(p)
}

//-----------------------------------------------------

//No. 4 Membuat Tangga
var segitiga='';
for(var i = 1; i < 8; i++){
    segitiga+='#';
    console.log(segitiga)
}

//-----------------------------------------------------
//No. 5 Membuat Papan Catur
for(var i = 0; i< 8;i++){
    var p='';
    if(i%2==0){
        for(var j = 0; j < 4; j++){
            p+=' #'
        }
    }else{
        for(var j = 0; j < 4; j++){
            p+='# '
        }
    }    
    console.log(p)
}



